#pragma once
#include <stdio.h>
#include <stdlib.h>
// william doyle 
// Febuary 17th 2020 

/* 
		The Stack is a FIFO (first in first out) data structure.
		Stacks are considered fundemental tools in programming and 
		algorithums. 
		I could not easily find a stack that was simple, versatile, and
		to my liking. With the hope that I never have to write a stack 
		again (we'll see) I wrote this.
		This stack stores void pointers. This allows you to use the stack
		to hold any data type. However it offers no type safty. Also you are
		required to cast to and from the void pointer type uppon pushing
		and poping.

		CREATING A STACK:
			struct stack * _stack_instance = make_stack();

		PUSHING AN INT:
			int number = 900;
			_stack_instance->push(_stack_instance, (void*)&number);

		POPING AN INT:
			int second_number;
			second_number = _stack_instance->pop(_stack_instance);

		DESTROYING A STACK:
			destroy_stack(_stack_instance);

		Note that destroying a stack does not free the data stored in it.
		Nor does poping it. In driver.c you can see that variables on the heap 
		(n_arr which is malloced) are freed on the line immedietly after the 
		stack is destoryed.
 */


struct lnode{
	void * data;
	struct lnode * next;
};

struct stack{
	struct lnode * top;
	void* (*pop)(struct stack *);
	void (*push)(struct stack *, void*);
};

// constructors
struct stack * make_stack (void);
struct lnode * make_lnode(void*);
// destructors
void destroy_lnode(struct lnode*);
void destroy_stack(struct stack*);

void __push(struct stack *, void * );
void* __pop (struct stack *);
