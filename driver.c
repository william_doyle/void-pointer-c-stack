#include "stack.h"
#include <time.h>
#include <pthread.h>

//	#define INT_TEST
//	#define EMPLOYEE_TEST
	#define THREAD_TEST

struct Employee {
	int ID;
	char * firstname;
	char * lastname;
	char * department;
	char hire_date[100];
	int pay_rate;
};

struct Employee * make_employee(char * fname, char * lname, char * deprt, int payrat){
	static int next_id = 0;
	struct Employee * self = malloc(sizeof(struct Employee));
	if (self == NULL){
		fprintf(stderr, "Error! Malloc failed in %s\n", __func__);
		exit(EXIT_FAILURE);
	}
	self->ID = next_id++;
	self->firstname = fname;
	self->lastname = lname;
	self->department = deprt;
	time_t now;
	struct tm * t = localtime(&now);
	sprintf(self->hire_date, "%02d %02d %04d %02d %02d", t->tm_mday, t->tm_mon+1, t->tm_year + 1900, t->tm_hour, t->tm_min);
	self->pay_rate = payrat;
	return self;	
}

int main (void){



#ifdef THREAD_TEST
	// emulate issue seen in james_the_snake/opengl_update_game.c

	struct Employee * boss;	
	boss = make_employee("HeapBoss", "dudename", "heap support", 0xC0FFEE);

	int * number = malloc(sizeof(int));
	*number = 500;

	struct stack * luggage = make_stack();
	
	// push some items to the stack
	luggage->push(luggage, (void*)number);
//	luggage->push(luggage, (void*)boss);


	luggage->pop(luggage);
//	luggage->pop(luggage);
//	luggage->pop(luggage);

	destroy_stack(luggage);
	free(number);
//	free(boss);

#endif















#ifdef EMPLOYEE_TEST
	struct stack * crew = make_stack();
	for (struct Employee * i = make_employee("James", "Kirk", "Bridge Crew", 0); i->ID < 10; i = make_employee("Spock", "OF VALKEN", "science", 0)){
		crew->push(crew, (void*)i);
	}
	
	destroy_stack(crew);
	return 1;
	for (struct Employee * i = crew->pop(crew); ; i = crew->pop(crew)){
		if (i == NULL){
			break;
		}
		printf("Name: %s ID: %d HIRE: %s\n",i->firstname , i->ID, i->hire_date);
		free(i);
	}
	//destroy_stack(crew);
#endif



#ifdef INT_TEST
	struct stack * mystack = make_stack();
	int one = 1;
	int two = 2;
	int * n_arr;

	n_arr = malloc(sizeof(int)*500);
	if (n_arr == NULL){
		exit(EXIT_FAILURE);
	}

	srand(1415926);

	for (int i = 0; i < 500; i++){
		n_arr[i] = rand()%100;
		mystack->push(mystack, (void *)&n_arr[i]);
	}	
	
	mystack->push( mystack, (void *)&two);
	mystack->push( mystack, (void *)&one);
	
	for (void * temp = mystack->pop(mystack); temp != NULL; temp = mystack->pop(mystack)){
		printf("Address %p Value: %d\n", temp, *(int*)temp);
	}

	mystack->push( mystack, (void *)&one);
	mystack->push( mystack, (void *)&two);
	
	printf("%d\n", *(int*)mystack->pop(mystack));
	printf("%d\n", *(int*)mystack->pop(mystack));
	
	destroy_stack(mystack);
	free(n_arr);
#endif

}
